# Website bourg-ville.ch - About

This is currently an outdated website: http://bourg-ville.ch/

The technology behind is old, the website is obsolete.

## New website technology requirements

The technology to be used in the new website is the following.

**Backend**
- django-cms (as non-programmers need to update each section of the website). So django-cms seems a good choice
- django-rest-framwork for API calls on the various models
- Unit tests!

**Frontend**
- just basic javascript and jquery
- No fancy react stuff.
- Only the framework svelte.js is authorized
- Bootstrap should be used for the CSS part

## Extra information
The website needs to also have interactions with some known food services

Unit test should be provided to valide the website works on fine.
The code should be put under my GitLab personal account project.

What I should be able to do to check locally:
$ pip install -r requirements
$ python manage runserver
and I should be able to see my website

## Important features
- I should be able to showcase a restaurant menu (people should be able to scroll on images)
- People should be able to order from a restaurant menu (once the order is passed, a form should apply with client's name, surname, phone number, address and other details). Once they validate the order, an email should be sent to me informing me that I received a new order. (no online payment required as client's pay on the go)
- some interaction with food service which I will provide is requested

## Quick summary of basic functionality requirements
More information on what I would like to have:
- A presentation page
- A gallery page
- A menu one can browse into such as in a real restaurant
- A menu page where people could select what items they would like to order and then submit they order. An email should be sent so the restaurant owner can get an email indicating who ordered and what. (take away)
- For people wanting their food to be delivered to their home, we use a service named gookay.
for orders etc. I would like to have django-rest-framework in the backend
for the models
- For the css style I would like to have bootstrap
- I'd like to have unit-tests for the API with django-rest-framework
- I will open a repository on GitLab under my name so you can commit to a branch (I hope you are familiar with Git)
- unit tests should be writtent as well

!!You need to build this site from scratch. I want it to be developed in django-cms not just pure django
as information need to be updated by people that are not developers.
For your information. Django-cms documentation can be found here: https://www.django-cms.org/en/
For the frontend I don't want React but either just pure javascript, jquery or if you could implement the frontend with svelte: https://svelte.dev/
